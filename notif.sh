#!/bin/bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage deploy.sh [STATUS] [MESSAGE] [TOKEN] [MESSAGE ID]"
    exit 1
fi


# get current branch and commit message
BRANCH=${BRANCH_NAME}
COMMIT_MESSAGE=${GIT_COMMIT_MSG}
APPLICATION=${SERVICE}
TIME=$(date -u +"%Y-%m-%d %H:%M:%S UTC")
STATUS=$1
MESSAGE=$2

# Token bot Telegram
BOT_TOKEN=$3
CHAT_ID=$4

# SEND notif by status
if [ "$STATUS" == "Failed" ]; then
MESSAGE_BODY=$(cat <<-END
🚨 *Deploy Notification*

*Status:* $STATUS
*Application:* $APPLICATION
*Branch:* $BRANCH
*Commit Message:* $COMMIT_MESSAGE
*Time:* $TIME
*Error Message:* $MESSAGE

*============*
*BUILD INFO*
*============*
*TAG:* ${BUILD_TAG}
*URL:* ${BUILD_URL}

Please check the logs and resolve the issues.

END
)

else 
MESSAGE_BODY=$(cat <<-END
🚀 *Deploy Notification*

*Status:* $STATUS
*Application:* $APPLICATION
*Branch:* $BRANCH
*Commit Message:* $COMMIT_MESSAGE
*Time:* $TIME
*Message:* $MESSAGE

*============*
*BUILD INFO*
*============*
*TAG:* ${BUILD_TAG}
*URL:* ${BUILD_URL}


Everything is running smoothly!

END
)
fi

# URL API Telegram
URL="https://api.telegram.org/bot$BOT_TOKEN/sendMessage"

# Mengirim pesan ke Telegram
curl -s -X POST $URL -d chat_id=$CHAT_ID -d text="$MESSAGE_BODY" -d parse_mode="Markdown"