#!/usr/bin/env bash

# # only for testing
# echo 'Create docker image'
# docker build -t $NAMESPACE:$TAG ./script
docker-compose -f ./script/docker-compose.yml up -d --build --always-recreate-deps
echo 'prune dangle image'
docker image prune -a -f