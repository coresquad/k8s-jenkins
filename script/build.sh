#!/usr/bin/env bash

## export go module
export GO111MODULE=on

## export gosumb
export GOSUMDB=off

cp -r ./migration ./script/migration
# chmod 600 script

echo 'Create binary file'
/usr/local/go/bin/go clean && CGO_ENABLED=0 /usr/local/go/bin/go build -o device-management
mv device-management ./script/device-management

