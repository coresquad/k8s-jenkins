#!/usr/bin/env bash
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
export GO111MODULE=on
export GOSUMDB=off

export GONOSUMDB="gitlab.com/coresquad/device-management/*"
export GOPRIVATE="gitlab.com/coresquad/device-management/*"
