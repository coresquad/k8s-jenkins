#!/usr/bin/env bash

## export go module
export GO111MODULE=on

## export gosumb
export GOSUMDB=off

# for mapping gitlab to private repo
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"

# private repo

echo 'Create binary file'
/usr/local/go/bin/go clean && CGO_ENABLED=0 /usr/local/go/bin/go build -o sayhello

chmod +x sayhello
