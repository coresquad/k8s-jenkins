#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage build_image.sh [NAMESPACE] [SERVICE] [TAG] [GROUP]"
    exit 1
fi
docker build  --no-cache -f script/k8s/Dockerfile -t  $4/$1/$2:$3 .
