#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage push_image.sh [NAMESPACE] [SERVICE] [TAG] [GROUP]"
    exit 1
fi

echo "push image"
docker tag $4/$1/$2:$3 dockhub.kotakkode.com/$4/$1/$2:$3
docker push dockhub.kotakkode.com/$4/$1/$2:$3
docker rmi $4/$1/$2:$3
