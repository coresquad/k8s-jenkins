#!/usr/bin/env bash

set -e
set -o pipefail

if [[ $# -eq 0 ]] 
    then
    echo "Usage deploy.sh [SERVICE] [NAMESPACE] [TAG] [KUBE_NAMESPACE] [GROUP]"
    exit 1
fi
echo "apply deployment"
# kubectl apply -n $3 -f - ===> apply with stdin
cat k8s/deployment.yaml | sed 's/\$SERVICE'"/$1/g" | sed 's/\$NAMESPACE'"/$2/g" | sed 's/\$TAG'"/$3/g" | sed 's/\$GROUP'"/$5/g"| kubectl apply -n $4 -f - --kubeconfig=kubeconfig.conf
kubectl apply -n $4 -f k8s/service.yaml --kubeconfig=kubeconfig.conf