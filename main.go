package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	getenv "github.com/aditya37/get-env"
	pb "github.com/aditya37/simple-rpc/proto"
	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func init() {
	os.Setenv("GRPC_GO_LOG_SEVERITY_LEVEL", "info")
	os.Setenv("GODEBUG", "http2debug=2")
}

type HelloService struct {
	pb.UnimplementedHelloServer
}

func (hs *HelloService) SayHello(context.Context, *empty.Empty) (*pb.ResponseSayHello, error) {
	return &pb.ResponseSayHello{
		Message: fmt.Sprintf("hello today is %s", time.Now().Format(time.RFC3339)),
	}, nil
}

func main() {
	address := fmt.Sprintf("%s:%d", getenv.GetString("SERVICE_ADDRESS", "0.0.0.0"), getenv.GetInt("PORT", 1387))
	list, err := net.Listen("tcp", address)
	if err != nil {
		log.Println(err)
		return
	}

	svr := grpc.NewServer()
	pb.RegisterHelloServer(svr, &HelloService{})
	reflection.Register(svr)

	log.Printf("listening at %v", list.Addr())

	if err := svr.Serve(list); err != nil {
		log.Println(err)
		return
	}

}
