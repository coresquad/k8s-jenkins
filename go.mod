module github.com/aditya37/simple-rpc

go 1.21.5

require (
	github.com/golang/protobuf v1.5.4
	google.golang.org/grpc v1.63.2
	google.golang.org/protobuf v1.34.1
)

require (
	github.com/aditya37/get-env v0.0.0-20220409152532-eba7a73ece1f // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240227224415-6ceb2ff114de // indirect
)
